---
title: Description
created: '2020-06-02T15:38:33.957Z'
modified: '2020-06-03T05:45:46.019Z'
---

### Description

Creating a user friendly lyrics website that is easy to see and navigate.

To see the UI designs of this page, the reference can be found at: https://bitbucket.org/eyvalon/ui-designs/src/master/Example%205/

# Example 5

![Alt Text](https://bitbucket.org/eyvalon/ui-designs/raw/5a770b0c35a71b8e23d9559c0179273aad9f5ca8/Example%205/Lyrics.png)

From using VueJS to build the website, we can see some changes shown below:

| Sample pictures| Sample pictures|
| -------------------------------------- | -------------------------------------- |
| ![Alt Text](./images/1.png) | ![Alt Text](./images/2.png) |
| ![Alt Text](./images/3.png) |  |

A few noticeable change is the implementation search system and home page that shows all current lyrics available, with using Plyr to handle video playing for YouTube and removal of bottom music player as the user is more likely to press on the Music Video player instead.

## Project setup

```
npm install
```

## Running project

To run on default port 8080:

```
npm run serve
```

To run on a certain port (i.e. 3000):

```
npm run serve -- --port 3000
```

