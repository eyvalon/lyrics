import Vue from "vue";
import App from "./App.vue";
import router from "./router";
import VuePlyr from "vue-plyr";

Vue.config.productionTip = false;

// The second argument is optional and sets the default config values for every player.
Vue.use(VuePlyr, {
  plyr: {
    fullscreen: { enabled: false }
  },
  emit: ["ended"]
});

new Vue({
  router,

  render: (h) => h(App)
}).$mount("#app");
