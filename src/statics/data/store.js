import Vue from "vue";

export const store = Vue.observable({
  songs: [
    {
      id: 0,
      album: {
        title: "Yesterday",
        artist: "Official 髭男dism (Official Higedandism)",
        year: 2019,
        language: "Japanese",
        genre: "Anime",
        lyric_by: "Satoshi Fujiwara (藤原聡 )",
        composed_by: "Satoshi Fujiwara (藤原聡 )",
        album_name: "Official HIGE DANdism"
      },

      lyrics: [
        {
          time: "0:15",
          romanji_lines: [
            { line: "Nando ushinattatte torikaeshite miseru yo" },
            {
              line: "Ameagari niji ga kakatta sora mitai na kimi no emi wo"
            },
            {
              line:
                "Tatoeba sono daishou ni dareka no hyoujou wo kumorasete shimattatte ii"
            },
            { line: "Warumono wa boku dake de ii" }
          ],
          english_lines: [
            {
              line:
                "No matter how many times you lose it, I'll get it back for you"
            },
            {
              line: "Your smile, like a rainbow in the sky after the rain"
            },
            {
              line: "Even if the price is darkening someone else's mood"
            },
            { line: "I’m okay with being the only bad guy" }
          ],
          isActive: false
        },
        {
          time: "0:43",
          romanji_lines: [
            { line: "Hontou wa itsudemo daremo to omoiyariatteitai" },
            {
              line: "Demo sonna yuuchou na risouron wa koko de sutenakucha na"
            }
          ],
          english_lines: [
            { line: "I want to feel compassion with everyone" },
            {
              line:
                "But I have to get rid of that easy ideal here How hard have I fought to wipe away"
            }
          ],
          isActive: false
        },
        {
          time: "1:00",
          romanji_lines: [
            { line: "Haruka saki de kimi e nerai wo sadameta kyoufu wo" },
            { line: "Dore dake boku wa haraikirerun darou?" },
            {
              line: "Hanshin hangi de sekentei ki ni shite bakka no iesutadei"
            },
            {
              line:
                "Poketto no naka de obieta kono te wa mada wasurerarenai mama"
            }
          ],
          english_lines: [
            {
              line: "The fear that has taken aim on you from such a distance?"
            },
            {
              line:
                "Yesterday I was so concerned with the world, only half sure"
            },
            {
              line: "I still can’t forget the fear I felt, hands in my pockets"
            }
          ],
          isActive: false
        },
        {
          time: "1:34",
          romanji_lines: [
            { line: '"Nando kizutsuitatte shikatanai yo" to itte' },
            { line: "Utsumuite kimi ga koboshita hakanaku namanurui namida" },
            {
              line: "Tada no hitotsubu datte boku wo fugainasa de oboresasete"
            },
            { line: "Risei wo ubau ni wa juubun sugita" }
          ],
          english_lines: [
            { line: '"You’ve hurt me so many times, it cannot be helped"' },
            { line: "You whispered, shedding warm, fleeting tears " },
            { line: "Just one is more than enough to shame me, drown me," },
            { line: "Rob me of my senses" }
          ],
          isActive: false
        },
        {
          time: "2:03",
          romanji_lines: [
            { line: "Machi no kurakushon mo sairen mo todoki ya shinai hodo" }
          ],
          english_lines: [
            { line: "So desperate that it drowns out horns and sirens" }
          ],
          isActive: false
        },
        {
          time: "2:10",
          romanji_lines: [
            { line: "Haruka saki e susume migatte sugiru koi da to" },
            { line: "Sekai ga ushiro kara yubisashite mo" },
            { line: "Furimukazu susume hisshi de kimi no moto e isogu yo" },
            { line: "Michi no tochuu de kikoeta SOS sae kizukanai furi de" }
          ],
          english_lines: [
            {
              line:
                "Moving on, far away – should the world point at me from behind and say it’s a selfish love"
            },
            {
              line: "I won’t look back, I’ll hurry to you with all my strength"
            },
            { line: "I’ll ignore any SOS I hear along the way" }
          ],
          isActive: false
        },
        {
          time: "3:11",
          romanji_lines: [
            { line: "Bye bye yesterday gomen ne" },
            { line: "Nagorioshii kedo yuku yo" },
            { line: "Itsuka no akogare to chigau boku demo" },
            { line: "Tada hitori dake kimi dake mamoru tame no tsuyosa wo" },
            { line: "Nani yori mo nozondeita kono te ni ima" }
          ],
          english_lines: [
            { line: "Bye-bye yesterday, I’m sorry" },
            { line: "It’s sad but I’m going" },
            { line: "I’m not what you had hoped for" },
            { line: "But now I have the strength to protect you, only you" },
            { line: "that I wished for more than anything" }
          ],
          isActive: false
        },
        {
          time: "3:42",
          romanji_lines: [
            { line: "Haruka saki e susume osana sugiru koi da to" },
            { line: "Sekai ga ushiro kara yubisashite mo" },
            { line: "Mayowazu ni susume susume futari dake no uchuu e to" },
            {
              line:
                "Ooketto no naka de furueta kono te de ima kimi wo tsuredashite"
            }
          ],
          english_lines: [
            {
              line:
                "Moving on, far away - should the world point at me from behind and say it’s a young love"
            },
            {
              line:
                "I’ll push on without hesitation, going forward, to a space with only the two of us"
            },
            {
              line:
                "My hands were shaking in my pockets, but now I’ll take you by the hand and lead you away"
            }
          ],
          isActive: false
        },
        {
          time: "4:13",
          romanji_lines: [
            { line: "Mirai no boku wa shiranai dakara shisen wa tomaranai" },
            { line: "Nazomeita hyougen gihou imishin na kimi no kishou" },
            { line: "I love you sae kaze ni tobasaresou na toki demo" },
            {
              line:
                "Bukiyou nagara tsunaida kono te wa mou kesshite hanasazu ni"
            },
            { line: "Niji no saki e" }
          ],
          english_lines: [
            {
              line:
                "I don’t know who I’ll be in the future, that’s why I’ll never stop my gaze"
            },
            { line: "Mysterious ways of expression - your deep nature" },
            {
              line:
                "Even in times when words of “I love you” are blown away by the wind"
            },
            {
              line:
                "I’ll take my clumsy hand and hold yours, and I’ll never let go"
            },
            { line: "As we head to the rainbow’s end" }
          ],
          isActive: false
        }
      ],

      items: [
        "1.   Opening Theme",
        "2.  Yowakina Naomi",
        "3.  Naomino Tanoshii Jikan",
        "5.  Nazono Otoko Nigerunaomi",
        "12. Yesterday - Movie Version",
        "17. A Time For Love",
        "23. Naomi vs Kitsunemen",
        "24. Kiss!?",
        "28. Rurio Torimodoshitai",
        "29. Kitsunemen Ibutsuno Haijo",
        "30.  Rurio Mamoru",
        "31.  Isoge! Okaidane!",
        "32.  Naomi Ruritono Wakare",
        "33.  Naomito Naomino Buddy",
        "34.  Kyuubiko",
        "35.  Lost Game - Movie Version",
        "36.  CREATION OF THE WORLD",
        "37.  HELLO WORLD",
        "38.  NEW WORLD"
      ],
      url_n: "https://i.ytimg.com/vi/9XkvjpBnudw/maxresdefault.jpg",
      videoId: "DuMqFknYHBs",
      musicLink:
        "https://f000.backblazeb2.com/b2api/v1/b2_download_file_by_id?fileId=4_z56d538e39139102d6a160216_f117e7e0a8b145a55_d20200602_m113234_c000_v0001049_t0059"
    },
    {
      id: 1,
      album: {
        title: "Lost Game",
        artist: "Official 髭男dism (Official Higedandism)",
        year: 2019,
        language: "Japanese",
        genre: "Anime",
        lyric_by: "Satoshi Fujiwara (藤原聡 )",
        composed_by: "Satoshi Fujiwara (藤原聡 )",
        album_name: "Official HIGE DANdism"
      },

      lyrics: [
        {
          time: "0:15",
          romanji_lines: [
            { line: "Nando ushinattatte torikaeshite miseru yo" },
            {
              line: "Ameagari niji ga kakatta sora mitai na kimi no emi wo"
            },
            {
              line:
                "Tatoeba sono daishou ni dareka no hyoujou wo kumorasete shimattatte ii"
            },
            { line: "Warumono wa boku dake de ii" }
          ],
          english_lines: [
            {
              line:
                "No matter how many times you lose it, I'll get it back for you"
            },
            {
              line: "Your smile, like a rainbow in the sky after the rain"
            },
            {
              line: "Even if the price is darkening someone else's mood"
            },
            { line: "I’m okay with being the only bad guy" }
          ],
          isActive: false
        },
        {
          time: "0:43",
          romanji_lines: [
            { line: "Hontou wa itsudemo daremo to omoiyariatteitai" },
            {
              line: "Demo sonna yuuchou na risouron wa koko de sutenakucha na"
            }
          ],
          english_lines: [
            { line: "I want to feel compassion with everyone" },
            {
              line:
                "But I have to get rid of that easy ideal here How hard have I fought to wipe away"
            }
          ],
          isActive: false
        },
        {
          time: "1:00",
          romanji_lines: [
            { line: "Haruka saki de kimi e nerai wo sadameta kyoufu wo" },
            { line: "Dore dake boku wa haraikirerun darou?" },
            {
              line: "Hanshin hangi de sekentei ki ni shite bakka no iesutadei"
            },
            {
              line:
                "Poketto no naka de obieta kono te wa mada wasurerarenai mama"
            }
          ],
          english_lines: [
            {
              line: "The fear that has taken aim on you from such a distance?"
            },
            {
              line:
                "Yesterday I was so concerned with the world, only half sure"
            },
            {
              line: "I still can’t forget the fear I felt, hands in my pockets"
            }
          ],
          isActive: false
        },
        {
          time: "1:34",
          romanji_lines: [
            { line: '"Nando kizutsuitatte shikatanai yo" to itte' },
            { line: "Utsumuite kimi ga koboshita hakanaku namanurui namida" },
            {
              line: "Tada no hitotsubu datte boku wo fugainasa de oboresasete"
            },
            { line: "Risei wo ubau ni wa juubun sugita" }
          ],
          english_lines: [
            { line: '"You’ve hurt me so many times, it cannot be helped"' },
            { line: "You whispered, shedding warm, fleeting tears " },
            { line: "Just one is more than enough to shame me, drown me," },
            { line: "Rob me of my senses" }
          ],
          isActive: false
        },
        {
          time: "2:03",
          romanji_lines: [
            { line: "Machi no kurakushon mo sairen mo todoki ya shinai hodo" }
          ],
          english_lines: [
            { line: "So desperate that it drowns out horns and sirens" }
          ],
          isActive: false
        },
        {
          time: "2:10",
          romanji_lines: [
            { line: "Haruka saki e susume migatte sugiru koi da to" },
            { line: "Sekai ga ushiro kara yubisashite mo" },
            { line: "Furimukazu susume hisshi de kimi no moto e isogu yo" },
            { line: "Michi no tochuu de kikoeta SOS sae kizukanai furi de" }
          ],
          english_lines: [
            {
              line:
                "Moving on, far away – should the world point at me from behind and say it’s a selfish love"
            },
            {
              line: "I won’t look back, I’ll hurry to you with all my strength"
            },
            { line: "I’ll ignore any SOS I hear along the way" }
          ],
          isActive: false
        },
        {
          time: "3:11",
          romanji_lines: [
            { line: "Bye bye yesterday gomen ne" },
            { line: "Nagorioshii kedo yuku yo" },
            { line: "Itsuka no akogare to chigau boku demo" },
            { line: "Tada hitori dake kimi dake mamoru tame no tsuyosa wo" },
            { line: "Nani yori mo nozondeita kono te ni ima" }
          ],
          english_lines: [
            { line: "Bye-bye yesterday, I’m sorry" },
            { line: "It’s sad but I’m going" },
            { line: "I’m not what you had hoped for" },
            { line: "But now I have the strength to protect you, only you" },
            { line: "that I wished for more than anything" }
          ],
          isActive: false
        },
        {
          time: "3:42",
          romanji_lines: [
            { line: "Haruka saki e susume osana sugiru koi da to" },
            { line: "Sekai ga ushiro kara yubisashite mo" },
            { line: "Mayowazu ni susume susume futari dake no uchuu e to" },
            {
              line:
                "Ooketto no naka de furueta kono te de ima kimi wo tsuredashite"
            }
          ],
          english_lines: [
            {
              line:
                "Moving on, far away - should the world point at me from behind and say it’s a young love"
            },
            {
              line:
                "I’ll push on without hesitation, going forward, to a space with only the two of us"
            },
            {
              line:
                "My hands were shaking in my pockets, but now I’ll take you by the hand and lead you away"
            }
          ],
          isActive: false
        },
        {
          time: "4:13",
          romanji_lines: [
            { line: "Mirai no boku wa shiranai dakara shisen wa tomaranai" },
            { line: "Nazomeita hyougen gihou imishin na kimi no kishou" },
            { line: "I love you sae kaze ni tobasaresou na toki demo" },
            {
              line:
                "Bukiyou nagara tsunaida kono te wa mou kesshite hanasazu ni"
            },
            { line: "Niji no saki e" }
          ],
          english_lines: [
            {
              line:
                "I don’t know who I’ll be in the future, that’s why I’ll never stop my gaze"
            },
            { line: "Mysterious ways of expression - your deep nature" },
            {
              line:
                "Even in times when words of “I love you” are blown away by the wind"
            },
            {
              line:
                "I’ll take my clumsy hand and hold yours, and I’ll never let go"
            },
            { line: "As we head to the rainbow’s end" }
          ],
          isActive: false
        }
      ],

      items: [
        "1.   Opening Theme",
        "2.  Yowakina Naomi",
        "3.  Naomino Tanoshii Jikan",
        "5.  Nazono Otoko Nigerunaomi",
        "12. Yesterday - Movie Version",
        "17. A Time For Love",
        "23. Naomi vs Kitsunemen",
        "24. Kiss!?",
        "28. Rurio Torimodoshitai",
        "29. Kitsunemen Ibutsuno Haijo",
        "30.  Rurio Mamoru",
        "31.  Isoge! Okaidane!",
        "32.  Naomi Ruritono Wakare",
        "33.  Naomito Naomino Buddy",
        "34.  Kyuubiko",
        "35.  Lost Game - Movie Version",
        "36.  CREATION OF THE WORLD",
        "37.  HELLO WORLD",
        "38.  NEW WORLD"
      ],
      url_n: "https://i.ytimg.com/vi/9XkvjpBnudw/maxresdefault.jpg",
      videoId: "DuMqFknYHBs",
      musicLink:
        "https://f000.backblazeb2.com/b2api/v1/b2_download_file_by_id?fileId=4_z56d538e39139102d6a160216_f117e7e0a8b145a55_d20200602_m113234_c000_v0001049_t0059"
    },
    {
      id: 2,
      album: {
        title: "A Time for Love",
        artist: "Official 髭男dism (Official Higedandism)",
        year: 2019,
        language: "Japanese",
        genre: "Anime",
        lyric_by: "Satoshi Fujiwara (藤原聡 )",
        composed_by: "Satoshi Fujiwara (藤原聡 )",
        album_name: "Official HIGE DANdism"
      },

      lyrics: [
        {
          time: "0:15",
          romanji_lines: [
            { line: "Nando ushinattatte torikaeshite miseru yo" },
            {
              line: "Ameagari niji ga kakatta sora mitai na kimi no emi wo"
            },
            {
              line:
                "Tatoeba sono daishou ni dareka no hyoujou wo kumorasete shimattatte ii"
            },
            { line: "Warumono wa boku dake de ii" }
          ],
          english_lines: [
            {
              line:
                "No matter how many times you lose it, I'll get it back for you"
            },
            {
              line: "Your smile, like a rainbow in the sky after the rain"
            },
            {
              line: "Even if the price is darkening someone else's mood"
            },
            { line: "I’m okay with being the only bad guy" }
          ],
          isActive: false
        },
        {
          time: "0:43",
          romanji_lines: [
            { line: "Hontou wa itsudemo daremo to omoiyariatteitai" },
            {
              line: "Demo sonna yuuchou na risouron wa koko de sutenakucha na"
            }
          ],
          english_lines: [
            { line: "I want to feel compassion with everyone" },
            {
              line:
                "But I have to get rid of that easy ideal here How hard have I fought to wipe away"
            }
          ],
          isActive: false
        },
        {
          time: "1:00",
          romanji_lines: [
            { line: "Haruka saki de kimi e nerai wo sadameta kyoufu wo" },
            { line: "Dore dake boku wa haraikirerun darou?" },
            {
              line: "Hanshin hangi de sekentei ki ni shite bakka no iesutadei"
            },
            {
              line:
                "Poketto no naka de obieta kono te wa mada wasurerarenai mama"
            }
          ],
          english_lines: [
            {
              line: "The fear that has taken aim on you from such a distance?"
            },
            {
              line:
                "Yesterday I was so concerned with the world, only half sure"
            },
            {
              line: "I still can’t forget the fear I felt, hands in my pockets"
            }
          ],
          isActive: false
        },
        {
          time: "1:34",
          romanji_lines: [
            { line: '"Nando kizutsuitatte shikatanai yo" to itte' },
            { line: "Utsumuite kimi ga koboshita hakanaku namanurui namida" },
            {
              line: "Tada no hitotsubu datte boku wo fugainasa de oboresasete"
            },
            { line: "Risei wo ubau ni wa juubun sugita" }
          ],
          english_lines: [
            { line: '"You’ve hurt me so many times, it cannot be helped"' },
            { line: "You whispered, shedding warm, fleeting tears " },
            { line: "Just one is more than enough to shame me, drown me," },
            { line: "Rob me of my senses" }
          ],
          isActive: false
        },
        {
          time: "2:03",
          romanji_lines: [
            { line: "Machi no kurakushon mo sairen mo todoki ya shinai hodo" }
          ],
          english_lines: [
            { line: "So desperate that it drowns out horns and sirens" }
          ],
          isActive: false
        },
        {
          time: "2:10",
          romanji_lines: [
            { line: "Haruka saki e susume migatte sugiru koi da to" },
            { line: "Sekai ga ushiro kara yubisashite mo" },
            { line: "Furimukazu susume hisshi de kimi no moto e isogu yo" },
            { line: "Michi no tochuu de kikoeta SOS sae kizukanai furi de" }
          ],
          english_lines: [
            {
              line:
                "Moving on, far away – should the world point at me from behind and say it’s a selfish love"
            },
            {
              line: "I won’t look back, I’ll hurry to you with all my strength"
            },
            { line: "I’ll ignore any SOS I hear along the way" }
          ],
          isActive: false
        },
        {
          time: "3:11",
          romanji_lines: [
            { line: "Bye bye yesterday gomen ne" },
            { line: "Nagorioshii kedo yuku yo" },
            { line: "Itsuka no akogare to chigau boku demo" },
            { line: "Tada hitori dake kimi dake mamoru tame no tsuyosa wo" },
            { line: "Nani yori mo nozondeita kono te ni ima" }
          ],
          english_lines: [
            { line: "Bye-bye yesterday, I’m sorry" },
            { line: "It’s sad but I’m going" },
            { line: "I’m not what you had hoped for" },
            { line: "But now I have the strength to protect you, only you" },
            { line: "that I wished for more than anything" }
          ],
          isActive: false
        },
        {
          time: "3:42",
          romanji_lines: [
            { line: "Haruka saki e susume osana sugiru koi da to" },
            { line: "Sekai ga ushiro kara yubisashite mo" },
            { line: "Mayowazu ni susume susume futari dake no uchuu e to" },
            {
              line:
                "Ooketto no naka de furueta kono te de ima kimi wo tsuredashite"
            }
          ],
          english_lines: [
            {
              line:
                "Moving on, far away - should the world point at me from behind and say it’s a young love"
            },
            {
              line:
                "I’ll push on without hesitation, going forward, to a space with only the two of us"
            },
            {
              line:
                "My hands were shaking in my pockets, but now I’ll take you by the hand and lead you away"
            }
          ],
          isActive: false
        },
        {
          time: "4:13",
          romanji_lines: [
            { line: "Mirai no boku wa shiranai dakara shisen wa tomaranai" },
            { line: "Nazomeita hyougen gihou imishin na kimi no kishou" },
            { line: "I love you sae kaze ni tobasaresou na toki demo" },
            {
              line:
                "Bukiyou nagara tsunaida kono te wa mou kesshite hanasazu ni"
            },
            { line: "Niji no saki e" }
          ],
          english_lines: [
            {
              line:
                "I don’t know who I’ll be in the future, that’s why I’ll never stop my gaze"
            },
            { line: "Mysterious ways of expression - your deep nature" },
            {
              line:
                "Even in times when words of “I love you” are blown away by the wind"
            },
            {
              line:
                "I’ll take my clumsy hand and hold yours, and I’ll never let go"
            },
            { line: "As we head to the rainbow’s end" }
          ],
          isActive: false
        }
      ],

      items: [
        "1.   Opening Theme",
        "2.  Yowakina Naomi",
        "3.  Naomino Tanoshii Jikan",
        "5.  Nazono Otoko Nigerunaomi",
        "12. Yesterday - Movie Version",
        "17. A Time For Love",
        "23. Naomi vs Kitsunemen",
        "24. Kiss!?",
        "28. Rurio Torimodoshitai",
        "29. Kitsunemen Ibutsuno Haijo",
        "30.  Rurio Mamoru",
        "31.  Isoge! Okaidane!",
        "32.  Naomi Ruritono Wakare",
        "33.  Naomito Naomino Buddy",
        "34.  Kyuubiko",
        "35.  Lost Game - Movie Version",
        "36.  CREATION OF THE WORLD",
        "37.  HELLO WORLD",
        "38.  NEW WORLD"
      ],
      url_n: "https://i.ytimg.com/vi/9XkvjpBnudw/maxresdefault.jpg",
      videoId: "DuMqFknYHBs",
      musicLink:
        "https://f000.backblazeb2.com/b2api/v1/b2_download_file_by_id?fileId=4_z56d538e39139102d6a160216_f117e7e0a8b145a55_d20200602_m113234_c000_v0001049_t0059"
    },
    {
      id: 3,
      album: {
        title: "Kiss?",
        artist: "Official 髭男dism (Official Higedandism)",
        year: 2019,
        language: "Japanese",
        genre: "Anime",
        lyric_by: "Satoshi Fujiwara (藤原聡 )",
        composed_by: "Satoshi Fujiwara (藤原聡 )",
        album_name: "Official HIGE DANdism"
      },

      lyrics: [
        {
          time: "0:15",
          romanji_lines: [
            { line: "Nando ushinattatte torikaeshite miseru yo" },
            {
              line: "Ameagari niji ga kakatta sora mitai na kimi no emi wo"
            },
            {
              line:
                "Tatoeba sono daishou ni dareka no hyoujou wo kumorasete shimattatte ii"
            },
            { line: "Warumono wa boku dake de ii" }
          ],
          english_lines: [
            {
              line:
                "No matter how many times you lose it, I'll get it back for you"
            },
            {
              line: "Your smile, like a rainbow in the sky after the rain"
            },
            {
              line: "Even if the price is darkening someone else's mood"
            },
            { line: "I’m okay with being the only bad guy" }
          ],
          isActive: false
        },
        {
          time: "0:43",
          romanji_lines: [
            { line: "Hontou wa itsudemo daremo to omoiyariatteitai" },
            {
              line: "Demo sonna yuuchou na risouron wa koko de sutenakucha na"
            }
          ],
          english_lines: [
            { line: "I want to feel compassion with everyone" },
            {
              line:
                "But I have to get rid of that easy ideal here How hard have I fought to wipe away"
            }
          ],
          isActive: false
        },
        {
          time: "1:00",
          romanji_lines: [
            { line: "Haruka saki de kimi e nerai wo sadameta kyoufu wo" },
            { line: "Dore dake boku wa haraikirerun darou?" },
            {
              line: "Hanshin hangi de sekentei ki ni shite bakka no iesutadei"
            },
            {
              line:
                "Poketto no naka de obieta kono te wa mada wasurerarenai mama"
            }
          ],
          english_lines: [
            {
              line: "The fear that has taken aim on you from such a distance?"
            },
            {
              line:
                "Yesterday I was so concerned with the world, only half sure"
            },
            {
              line: "I still can’t forget the fear I felt, hands in my pockets"
            }
          ],
          isActive: false
        },
        {
          time: "1:34",
          romanji_lines: [
            { line: '"Nando kizutsuitatte shikatanai yo" to itte' },
            { line: "Utsumuite kimi ga koboshita hakanaku namanurui namida" },
            {
              line: "Tada no hitotsubu datte boku wo fugainasa de oboresasete"
            },
            { line: "Risei wo ubau ni wa juubun sugita" }
          ],
          english_lines: [
            { line: '"You’ve hurt me so many times, it cannot be helped"' },
            { line: "You whispered, shedding warm, fleeting tears " },
            { line: "Just one is more than enough to shame me, drown me," },
            { line: "Rob me of my senses" }
          ],
          isActive: false
        },
        {
          time: "2:03",
          romanji_lines: [
            { line: "Machi no kurakushon mo sairen mo todoki ya shinai hodo" }
          ],
          english_lines: [
            { line: "So desperate that it drowns out horns and sirens" }
          ],
          isActive: false
        },
        {
          time: "2:10",
          romanji_lines: [
            { line: "Haruka saki e susume migatte sugiru koi da to" },
            { line: "Sekai ga ushiro kara yubisashite mo" },
            { line: "Furimukazu susume hisshi de kimi no moto e isogu yo" },
            { line: "Michi no tochuu de kikoeta SOS sae kizukanai furi de" }
          ],
          english_lines: [
            {
              line:
                "Moving on, far away – should the world point at me from behind and say it’s a selfish love"
            },
            {
              line: "I won’t look back, I’ll hurry to you with all my strength"
            },
            { line: "I’ll ignore any SOS I hear along the way" }
          ],
          isActive: false
        },
        {
          time: "3:11",
          romanji_lines: [
            { line: "Bye bye yesterday gomen ne" },
            { line: "Nagorioshii kedo yuku yo" },
            { line: "Itsuka no akogare to chigau boku demo" },
            { line: "Tada hitori dake kimi dake mamoru tame no tsuyosa wo" },
            { line: "Nani yori mo nozondeita kono te ni ima" }
          ],
          english_lines: [
            { line: "Bye-bye yesterday, I’m sorry" },
            { line: "It’s sad but I’m going" },
            { line: "I’m not what you had hoped for" },
            { line: "But now I have the strength to protect you, only you" },
            { line: "that I wished for more than anything" }
          ],
          isActive: false
        },
        {
          time: "3:42",
          romanji_lines: [
            { line: "Haruka saki e susume osana sugiru koi da to" },
            { line: "Sekai ga ushiro kara yubisashite mo" },
            { line: "Mayowazu ni susume susume futari dake no uchuu e to" },
            {
              line:
                "Ooketto no naka de furueta kono te de ima kimi wo tsuredashite"
            }
          ],
          english_lines: [
            {
              line:
                "Moving on, far away - should the world point at me from behind and say it’s a young love"
            },
            {
              line:
                "I’ll push on without hesitation, going forward, to a space with only the two of us"
            },
            {
              line:
                "My hands were shaking in my pockets, but now I’ll take you by the hand and lead you away"
            }
          ],
          isActive: false
        },
        {
          time: "4:13",
          romanji_lines: [
            { line: "Mirai no boku wa shiranai dakara shisen wa tomaranai" },
            { line: "Nazomeita hyougen gihou imishin na kimi no kishou" },
            { line: "I love you sae kaze ni tobasaresou na toki demo" },
            {
              line:
                "Bukiyou nagara tsunaida kono te wa mou kesshite hanasazu ni"
            },
            { line: "Niji no saki e" }
          ],
          english_lines: [
            {
              line:
                "I don’t know who I’ll be in the future, that’s why I’ll never stop my gaze"
            },
            { line: "Mysterious ways of expression - your deep nature" },
            {
              line:
                "Even in times when words of “I love you” are blown away by the wind"
            },
            {
              line:
                "I’ll take my clumsy hand and hold yours, and I’ll never let go"
            },
            { line: "As we head to the rainbow’s end" }
          ],
          isActive: false
        }
      ],

      items: [
        "1.   Opening Theme",
        "2.  Yowakina Naomi",
        "3.  Naomino Tanoshii Jikan",
        "5.  Nazono Otoko Nigerunaomi",
        "12. Yesterday - Movie Version",
        "17. A Time For Love",
        "23. Naomi vs Kitsunemen",
        "24. Kiss!?",
        "28. Rurio Torimodoshitai",
        "29. Kitsunemen Ibutsuno Haijo",
        "30.  Rurio Mamoru",
        "31.  Isoge! Okaidane!",
        "32.  Naomi Ruritono Wakare",
        "33.  Naomito Naomino Buddy",
        "34.  Kyuubiko",
        "35.  Lost Game - Movie Version",
        "36.  CREATION OF THE WORLD",
        "37.  HELLO WORLD",
        "38.  NEW WORLD"
      ],
      url_n: "https://i.ytimg.com/vi/9XkvjpBnudw/maxresdefault.jpg",
      videoId: "DuMqFknYHBs",
      musicLink:
        "https://f000.backblazeb2.com/b2api/v1/b2_download_file_by_id?fileId=4_z56d538e39139102d6a160216_f117e7e0a8b145a55_d20200602_m113234_c000_v0001049_t0059"
    }
  ]
});

export const mutations = {};
