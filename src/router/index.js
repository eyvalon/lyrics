import Vue from "vue";
import VueRouter from "vue-router";
import Home from "../components/Home.vue";
import PageNotFound from "../components/PageError.vue";
import SearchResults from "../components/searchResult.vue";
import lyrics from "../components/Lyrics.vue";

Vue.use(VueRouter);

const router = new VueRouter({
  mode: "history",
  base: process.env.BASE_URL,
  routes: [
    {
      path: "/",
      name: "Home",
      component: Home
    },
    {
      path: "/search/:tag",
      name: "searchResults",
      component: SearchResults,
      props: true
    },
    {
      path: "/lyrics/:dataList",
      name: "currentPage",
      component: lyrics
    },
    {
      path: "/404",
      component: PageNotFound
    },

    { path: "*", redirect: "/404" }
  ]
});

export default router;
